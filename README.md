# generic-binary-encoding

This library provides generic functions for binary encoding and decoding.

## Licence

The package is licensed under the BSD-2-Clause license; for details, see the
[LICENSE](/LICENSE) file.
