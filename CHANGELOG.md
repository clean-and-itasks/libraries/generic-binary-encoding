# Changelog

#### 1.0.2

- Chore: support base `3.0`.

#### 1.0.1

- Enhancement: Prevent abort in `gBinaryDecode` on encoding with invalid constructor index.

## 1.0.0

- Initial version, import modules from clean platform v0.3.38 and destill all
  generic binary encoding modules.
